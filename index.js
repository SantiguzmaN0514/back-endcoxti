'use strict'

var app = require('./app');
var port = 3789;

app.listen(port, () => {
	console.log("El servidor local con Node y Express está corriendo correctamente en el puerto:", port)
});
