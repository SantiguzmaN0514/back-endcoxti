'use strict'

var express = require('express');
var UserController = require('../controllers/user');

var api = express.Router();
var multipart = require('connect-multiparty');

api.post('/register', UserController.saveUser);
api.post('/login', UserController.login);

module.exports = api;
