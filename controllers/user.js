'use strict'
const { Pool } = require('pg')

const pool = new Pool({
	user: 'postgres',
	password: 'sssss',
	host: 'localhost',
	database: 'postgres',
	port: '5433'
});

const saveUser = async(req, res) => {
	const {
		id,
		nombre,
		celular,
		correo,
		password,
		departamento,
		ciudad,
		barrio,
		direccion,
		salario,
		otrosingresos,
		gastosmensuales,
		gastosfinancieros
	} = req.body;
	let response = '';
	try {
		response = await pool.query(
			'INSERT INTO users (id, nombre, celular, correo, password, departamento, ciudad, '+
			'barrio, direccion, salario, otrosingresos, gastosmensuales, gastosfinancieros)'+
			'VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13)',
			[
				id,
				nombre,
				celular,
				correo,
				password,
				departamento,
				ciudad,
				barrio,
				direccion,
				salario,
				otrosingresos,
				gastosmensuales,
				gastosfinancieros
			]
		);
	} catch (error) {
		response = error;
	}
	res.status(200).send(response);
};

const login = async(req, res) => {
	const { correo, password } = req.body;
	let response = {};
	try {
		response = await pool.query(
			'SELECT salario FROM users WHERE correo = $1 AND password = $2', [correo, password]
		);
	} catch (error) {
		response = 'error en la consulta';
	}
	if (response.rowCount > 0) {
		res.status(200).send({salario: response.rows[0].salario});
	} else {
		res.status(200).send({
			message: 'NoExiste'
		});
	}
}

module.exports = {
	saveUser,
	login
};