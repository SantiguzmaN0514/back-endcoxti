# README

el backend tiene una buena estructura por carpetas

1. Controllers: en esta carpeta se manejan los metodos para aceptar los peticiones generadas desde el front-end.
2. Router: en esta carpeta se encuentran generadas las rutas para los request. para poder consumir los controllers.

tambien se tiene un archivo  app.js donde se importan las rutas y y se crean los headers.

En este backend se instalo la libreria eslint. desde la cual se estructuro y se ordeno el codifo de una manera apropiada.

Tambien se creo una conexion a la Base de Datos creada en postgres tanto para registro como para el login.
